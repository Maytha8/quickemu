Source: quickemu
Section: utils
Priority: optional
Maintainer: Maytham Alsudany <maytham@debian.org>
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 pandoc,
Standards-Version: 4.7.2
Homepage: https://github.com/quickemu-project/quickemu
Vcs-Browser: https://salsa.debian.org/Maytha8/quickemu
Vcs-Git: https://salsa.debian.org/Maytha8/quickemu.git

Package: quickemu
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 genisoimage,
 jq,
 procps,
 python3,
 qemu-system-x86,
 qemu-utils,
 spice-client-gtk,
 swtpm,
 unzip,
 usbutils,
 wget,
 xdg-user-dirs,
 zsync,
Description: quickly create and run optimised virtual machines
 Quickemu is a wrapper for the excellent QEMU that attempts to automatically
 "do the right thing", rather than expose exhaustive configuration options.
 .
 Quickly create and run optimised Windows, macOS and Linux desktop virtual
 machines; with just two commands. You decide what operating system you want to
 run and Quickemu will figure out the best way to do it for you. For example:
 .
   quickget ubuntu-mate 22.04
   quickemu --vm ubuntu-mate-22.04.conf
 .
 The original objective of the project was to enable quick testing of Linux
 distributions where the virtual machine configurations can be stored anywhere,
 such as external USB storage or your home directory, and no elevated
 permissions are required to run the virtual machines. Quickemu now also
 includes comprehensive support for macOS and Windows.
